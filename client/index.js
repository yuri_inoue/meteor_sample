////////// Main client application logic //////////

var title = 'Node.js検証用 テーマ管理';

Template.header.title = function () {
  return title;
};

Template.test.title = function () {
  return title;
};

Template.test.theme = function () {
//  var theme = Themes.findOne({_id: {$eq: 1});
//  return theme;
    return 'Best of Myラーメン';
};


Template.test.events({
  'click input#save': function () {
    var id = $('#theme_id').html().trim();
//    var theme = $('input#theme_text').val().trim();
//    Themes.update(id, {$set: {theme_text: theme}});
//    Meteor.call('start_new_game');
    console.log('id is '+id);
  }
});



//////
////// Initialization
//////

Meteor.startup(function () {
  // Allocate a new player id.
  //
  // XXX this does not handle hot reload. In the reload case,
  // Session.get('player_id') will return a real id. We should check for
  // a pre-existing player, and if it exists, make sure the server still
  // knows about us.
//  var player_id = Players.insert({name: '', idle: false});
//  Session.set('player_id', player_id);

  // subscribe to all the players, the game i'm in, and all
  // the words in that game.
//  Deps.autorun(function () {
//    Meteor.subscribe('players');

//    if (Session.get('player_id')) {
//      var me = player();
//      if (me && me.game_id) {
//        Meteor.subscribe('games', me.game_id);
//        Meteor.subscribe('words', me.game_id, Session.get('player_id'));
//      }
//    }
//  });

  // send keepalives so the server can tell when we go away.
  //
  // XXX this is not a great idiom. meteor server does not yet have a
  // way to expose connection status to user code. Once it does, this
  // code can go away.
//  Meteor.setInterval(function() {
//    if (Meteor.status().connected)
//      Meteor.call('keepalive', Session.get('player_id'));
//  }, 20*1000);
});
